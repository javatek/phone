package io.bitbucket.javatek.phone;

import javax.annotation.Nonnull;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;

/**
 * Phone number
 */
public final class Phone {
  /**
   * Country code
   */
  private final String country;

  /**
   * City code / mobile code
   */
  private final String city;

  /**
   * Subscriber number
   */
  private final List<String> num;

  public Phone(@Nonnull String number) {
    Matcher matcher = PhonePattern.RUSSIAN_PATTERN.matcher(number);
    if (matcher.matches()) {
      this.country = normalizeCountry(matcher.group(1).trim());
      this.city = matcher.group(3).replace("(", "").replace(")", "").trim();
      this.num =
        Arrays.asList(
          matcher.group(6).trim(),
          matcher.group(7).trim(),
          matcher.group(8).trim()
        );
      return;
    }

    matcher = PhonePattern.FRANCE_PATTERN.matcher(number);
    if (matcher.matches()) {
      this.country = normalizeCountry(matcher.group(1).trim());
      this.city = matcher.group(3).replace("(", "").replace(")", "").trim();
      this.num =
        Arrays.asList(
          matcher.group(6).trim(),
          matcher.group(7).trim(),
          matcher.group(8).trim(),
          matcher.group(9).trim()
        );
      return;
    }

    matcher = PhonePattern.UAE_PATTERN.matcher(number);
    if (matcher.matches()) {
      this.country = normalizeCountry(matcher.group(1).trim());
      this.city = matcher.group(2).replace("(", "").replace(")", "").trim();
      this.num =
        Arrays.asList(
          matcher.group(5).trim(),
          matcher.group(6).trim()
        );
      return;
    }

    throw new BadPhoneException(
      MessageFormat.format("{0} is not a phone number", number)
    );
  }

  private String normalizeCountry(String country) {
    return country.equals("8") ? "+7" : country;
  }

  /**
   * @return city code with "+" char if presented
   */
  @Nonnull
  public String country() {
    return country;
  }

  @Nonnull
  public String city() {
    return city;
  }

  @Nonnull
  public String number() {
    return String.join("", num);
  }

  @Override
  public int hashCode() {
    return Objects.hash(num);
  }

  @Override
  public boolean equals(Object other) {
    return other == this || (
      other != null
        && other.getClass() == Phone.class
        && ((Phone) other).country.equals(this.country)
        && ((Phone) other).city.equals(this.city)
        && ((Phone) other).num.equals(this.num)
    );
  }

  @Override
  public String toString() {
    return
      country
        + " " + city
        + " " + String.join(" ", num)
      ;
  }
}
