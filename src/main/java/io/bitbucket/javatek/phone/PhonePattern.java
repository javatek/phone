package io.bitbucket.javatek.phone;

import java.util.regex.Pattern;

/**
 *
 */
interface PhonePattern {
  Pattern RUSSIAN_PATTERN = Pattern.compile(
    "^" +
      "((\\+[1-9][0-9]{0,2})|8)" +                        // country code
      "\\s*" +
      "((\\(\\s*[0-9]{3,4}\\s*\\))|([0-9]{3,4}\\s*-?))" + // city / locality code
      "\\s*" +
      "([0-9]{3})" +                                      // number first 3 digits
      "\\s*-?\\s*" +
      "([0-9]{2})" +                                      // number second 2 digits
      "\\s*-?\\s*" +
      "([0-9]{2})" +                                      // number last 2 digits
      "$"
  );

  Pattern FRANCE_PATTERN = Pattern.compile(
    "^" +
      "((\\+[1-9][0-9]{0,2})|8)" +                        // country code
      "\\s*" +
      "((\\(\\s*[0-9]{1,2}\\s*\\))|([0-9]{1,2}\\s*-?))" + // city / mobile code
      "\\s*" +
      "([0-9]{2})" +                                      // number first 2 digits
      "\\s*-?\\s*" +
      "([0-9]{2})" +                                      // number second 2 digits
      "\\s*-?\\s*" +
      "([0-9]{2})" +                                      // number third 2 digits
      "\\s*-?\\s*" +
      "([0-9]{2})" +                                      // number last 2 digits
      "$"
  );

  Pattern UAE_PATTERN = Pattern.compile(
    "^" +
      "(\\+971)" +                                        // CC - country code
      "\\s*" +
      "((\\(\\s*[0-9]{1,2}\\s*\\))|([0-9]{1,2}\\s*-?))" + // city / mobile code
      "\\s*" +
      "([0-9]{3})" +                                      // number first 3 digits
      "\\s*-?\\s*" +
      "([0-9]{2}\\s*-?\\s*[0-9]{2})" +                    // number second 4 digits
      "$"
  );
}
