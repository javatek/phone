package io.bitbucket.javatek.phone;

/**
 *
 */
public final class BadPhoneException extends IllegalArgumentException {
  BadPhoneException(String message) {
    super(message);
  }
}
