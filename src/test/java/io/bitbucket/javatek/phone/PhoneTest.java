package io.bitbucket.javatek.phone;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PhoneTest {
  @Test
  public void constructGoodPhone1() {
    Phone phone = new Phone("+7 903 204 81 19");
    assertEquals("+7", phone.country());
    assertEquals("903", phone.city());
    assertEquals("2048119", phone.number());
    assertEquals("+7 903 204 81 19", phone.toString());
  }

  @Test
  public void constructGoodFrancePhone1() {
    Phone phone = new Phone("+33 6 95 21 23 77");
    assertEquals("+33", phone.country());
    assertEquals("6", phone.city());
    assertEquals("95212377", phone.number());
    assertEquals("+33 6 95 21 23 77", phone.toString());
  }

  @Test
  public void constructGoodPhone2() {
    Phone phone = new Phone("8495 123-91-21");
    assertEquals("+7", phone.country());
    assertEquals("495", phone.city());
    assertEquals("1239121", phone.number());
    assertEquals("+7 495 123 91 21", phone.toString());
  }

  @Test
  public void constructGoodPhone3() {
    Phone phone = new Phone("+99 (331) 123 91 21");
    assertEquals("+99", phone.country());
    assertEquals("331", phone.city());
    assertEquals("1239121", phone.number());
    assertEquals("+99 331 123 91 21", phone.toString());
  }

  @Test
  public void constructUAEGoodPhone() {
    Phone phone = new Phone("+971 50 729 1451");
    assertEquals("+971", phone.country());
    assertEquals("50", phone.city());
    assertEquals("7291451", phone.number());
    assertEquals("+971 50 729 1451", phone.toString());
  }
}